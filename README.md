# README #

1/3スケールのゲームセンターに置いてある椅子風小物のstlファイルです。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK Fusion360です。


***

# 実機情報

## メーカ
- -

## 発売時期

- -

## 参考資料

- -

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_game_center_chair/raw/e85960787bee8e71449f73867ebc70cf74711da4/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_game_center_chair/raw/e85960787bee8e71449f73867ebc70cf74711da4/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_game_center_chair/raw/e85960787bee8e71449f73867ebc70cf74711da4/ExampleImage.png)
